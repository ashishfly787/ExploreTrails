//
//  CardView.swift
//  Hike
//
//  Created by Ashish Stephen on 9/6/23.
//

import SwiftUI

@available(iOS 16.0, *)
struct CardView: View {
    var body: some View {
        // MARK: - CARD
        ZStack {
            CustomBackgroundView()
            
            VStack(alignment:.leading) {
                //MARK: - Header
                VStack {
                    HStack {
                        Text("Explor")
                            .fontWeight(.black)
                            .font(.system(size: 52))
                            .foregroundStyle(
                                LinearGradient(colors: [.customGrayMedium, .customGrayLight], startPoint: .top, endPoint: .bottom)
                        )
                        Spacer()
                        
                        Button {
                            // ACTION: Show a sheet
                            print("Button-log")
                        } label: {
                            CustomButtonView()
                        }
                    }
                    Text("Vist the unknown and find new meaning and paths to life")
                        .multilineTextAlignment(.leading)
                        .italic()
                        .foregroundColor(.customGrayMedium)
                }
                .padding(.horizontal,30)
                // MARK: - Content
                
                ZStack {
                    Circle()
                        .fill(
                            LinearGradient(colors: [Color("ColorIndigoMedium"),
                                                   Color("ColorSalmonLight")], startPoint: .topLeading, endPoint: .bottomTrailing))
                        .frame(width: 256, height: 256)
                    
                    Image("image-1")
                        .resizable()
                    .scaledToFit()
                }
                
                // MARK: Footer
            }
        }
        .frame(width: 320, height: 550)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 16.0, *) {
            CardView()
        } else {
            // Fallback on earlier versions
        }
    }
}
