//
//  CustomButtonView.swift
//  Hike
//
//  Created by Ashish Stephen on 9/7/23.
//

import SwiftUI

@available(iOS 16.0, *)
struct CustomButtonView: View {
    var body: some View {
        ZStack{
            Circle()
                .fill(LinearGradient(colors: [ .customGreenMedium, .customGreenDark,.customGrayLight], startPoint: .top, endPoint: .bottom))
            
            Circle()
                .stroke(LinearGradient(colors: [.customGrayLight, .customGrayMedium], startPoint: .top, endPoint: .bottom))
            
            Image(systemName: "figure.hiking")
                .fontWeight(.black)
                .font(.system(size: 30))
                .foregroundStyle(LinearGradient(colors: [ .customGrayLight,.customGrayMedium], startPoint: .top, endPoint: .bottom))
        }
        .frame(width: 58, height: 58)
    }
}

struct CustomButtonView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 16.0, *) {
                CustomButtonView()
                .previewLayout(.sizeThatFits)
                .padding()
            } else {
                // Fallback on earlier versions
            }
        }
    }

