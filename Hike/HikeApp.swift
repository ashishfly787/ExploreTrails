//
//  HikeApp.swift
//  Hike
//
//  Created by Ashish Stephen on 9/6/23.
//

import SwiftUI

@main
struct HikeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
